package com.cls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//Collection data1 = new ArrayList();
		
		//List data1 = new ArrayList();
		
		ArrayList data = new ArrayList();
		
		data.add(10);
		data.add(737.38f);
		data.add(23.33);
		data.add('A');
		data.add("java");
		data.add("java");
		data.add(10);
		data.add(false);
		data.add(new Object());
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		System.out.println(data.get(6));
		
		System.out.println(data.lastIndexOf(false));
		
		data.clear();
		
		//Iterator --> ListIterator
		
		//Iterator itr = data.iterator();
		
		ListIterator itr = data.listIterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		System.out.println("==========");
		
		while(itr.hasPrevious())
		{
			System.out.println(itr.previous());
		}
		
		LinkedList data1 = new LinkedList();
		
		data1.add(10);
		data1.add(737.38f);
		data1.add(23.33);
		data1.add('A');
		data1.add("java");
		data1.add("java");
		data1.add(10);
		data1.add(false);
		data1.add(new Object());
		
		System.out.println(data1);
	}

}
