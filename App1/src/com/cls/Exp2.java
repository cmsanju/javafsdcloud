package com.cls;

import java.util.Iterator;
import java.util.Stack;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(10);
		data.add(737.38f);
		data.add(23.33);
		data.add('A');
		data.add("java");
		data.add("java");
		data.add(10);
		data.add(false);
		
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		System.out.println(data.pop());
		
		System.out.println(data);
		
		data.push("hello");
		
		System.out.println(data.search(100));
		
		System.out.println(data.empty());
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
