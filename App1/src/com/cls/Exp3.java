package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class Exp3 {
	
	public static void main(String[] args) {
		
		HashSet data = new HashSet();
		
		data.add(10);
		data.add(737.38f);
		data.add(23.33);
		data.add('A');
		data.add("java");
		data.add("java");
		data.add(10);
		data.add(false);
		
		
		System.out.println(data);
		
		LinkedHashSet data1 = new LinkedHashSet();
		
		data1.add(10);
		data1.add(737.38f);
		data1.add(23.33);
		data1.add('A');
		data1.add("java");
		data1.add("java");
		data1.add(10);
		data1.add(false);
		
		System.out.println(data1);
		
		Iterator itr = data1.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
