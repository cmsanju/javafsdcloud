package com.cls;

import java.util.Iterator;
import java.util.TreeSet;

public class Exp4 {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> data = new TreeSet<>();
		
		data.add(10);
		data.add(20);
		data.add(1);
		data.add(6);
		data.add(9);
		data.add(7);
		data.add(10);
		data.add(11);
		
		System.out.println(data);
		
		TreeSet<String> data1 = new TreeSet<String>();
		
		data1.add("hello");
		data1.add("java");
		data1.add("apple");
		data1.add("asus");
		data1.add("dell");
		data1.add("add");
		data1.add("cash");
		data1.add("book");
		
		System.out.println(data1);
		
		Iterator<String> itr = data1.descendingIterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
	}

}
