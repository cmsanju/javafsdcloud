package com.cls;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class Exp5 {
	
	public static void main(String[] args) {
		
		//HashMap<String, Integer> data = new HashMap<>();
		
		//LinkedHashMap<String, Integer> data = new LinkedHashMap<>();
		
		Hashtable<String, Integer> data = new Hashtable<String, Integer>();
		
		data.put("java", 2233);
		
		data.put("java", 3344);
		data.put("net", 6767);
		data.put("python", 8448);
		data.put("spring", 3453);
		data.put("hibernate", 4489);
		
		System.out.println(data);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Key : "+et.getKey()+" Value : "+et.getValue());
		}
		
		for(String key : data.keySet())
		{
			System.out.println("Key : "+key+" value : "+data.get(key));
			
			if(key.contains("net"))
			{
				System.out.println("contains .net");
				break;
			}
		}
		
		System.out.println(data.size());
		data.clear();
	}

}
