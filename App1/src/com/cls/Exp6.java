package com.cls;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.Map.Entry;

public class Exp6 {
	
	public static void main(String[] args) {
		
		TreeMap<Integer, String> data = new TreeMap<>(); 
		
		data.put(1212, "lenovo");
		data.put(2233, "dell");
		data.put(3434, "sony");
		data.put(4545, "asus");
		data.put(2121, "ideapad");
		data.put(4545, "think pad");
		
		System.out.println(data);
		
		TreeMap<String, Integer> data1 = new TreeMap<>();
		
		data1.put("lenovo", 1212);
		data1.put("dell", 2323);
		data1.put("sony", 9877);
		data1.put("idea pad", 8373);
		data1.put("think pad", 830);
		data1.put("asus", 345);
		
		
		System.out.println(data1);
		
		Iterator<Entry<String, Integer>> itr = data1.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Product : "+et.getKey()+" Price : "+et.getValue());
		}
	}

}
