package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//step 1 load the driver class
		Class.forName("com.mysql.jdbc.Driver");//oracle.jdbc.driver.OracleDriver
		
		//step 2 create connection object      //jdbc:oralce:thin:@localhost:1521:xe, "system", "java"
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mng11", "root", "password");
		con.setAutoCommit(false);
		//step 3 create statement object
		Statement stmt = con.createStatement();
		
		//step 4 execute query
		//stmt.execute("create table abc1(id int, name varchar(50), city varchar(50))");
		
		String sql = "insert into abc1 values(5, 'Apple', 'Blr')";
		stmt.addBatch(sql);
		stmt.addBatch("insert into abc1 values(3, 'Hello', 'BTM')");
		stmt.addBatch("insert into abc1 values(4, 'user', 'Blr')");
		
		//stmt.execute(sql);
		stmt.executeBatch();
		//step 5 close the connection object
		//con.commit();
		con.rollback();
		con.close();
		
		System.out.println("Done.");
	}

}
