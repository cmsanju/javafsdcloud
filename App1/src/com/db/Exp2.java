package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Exp2 {
	
	public static void main(String[] args)throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mng11", "root", "password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into abc1 values(?,?,?)");
		
		pst.setInt(1, 3);
		pst.setString(2, "Spring");
		pst.setString(3, "Blr");
		
		pst.execute();
		
		
		PreparedStatement pst = con.prepareStatement("update abc1 set name = ?, city = ? where id = ?");
		
		pst.setString(1, "Hibernate");
		pst.setString(2, "Mpl");
		pst.setInt(3, 3);
		
		pst.execute();
		
		
		PreparedStatement pst = con.prepareStatement("delete from abc1 where id = ?");
		
		pst.setInt(1, 3);
		pst.execute();
		*/
		
		PreparedStatement pst = con.prepareStatement("select * from abc1");
		
		ResultSet rs = pst.executeQuery();
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		
	}

}
