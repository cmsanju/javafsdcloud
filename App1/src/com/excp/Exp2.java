package com.excp;

public class Exp2 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println("Test");
			
			System.out.println(20/5);
			
			System.out.println("HELLO");
			
			String name = "java";
			
			System.out.println(name.charAt(3));
			
			int[] ar = {23,40,56,90};
			
			System.out.println(ar[2]);
			
			String str = null;
			
			System.out.println(str.charAt(0));
		}
		catch(ArithmeticException e)
		{
			System.out.println("Can't divided by zero");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check your name length");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check array size");
		}
		catch(NullPointerException npe)
		{
			System.out.println("please enter input for string");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		
		finally
		{
			System.out.println("I am from finally.");
		}
		
	}

}
/*
   1 try block
   2 catch block
   3 finally block
   4 throws
   5 throw
*/