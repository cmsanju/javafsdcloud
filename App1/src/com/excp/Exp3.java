package com.excp;

public class Exp3 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(40/0);
		}
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(Exception e)
		{
			//1 using getMessage()
			
			System.out.println(e.getMessage());
			
			//2 printing exception class object
			
			System.out.println(e);
			
			//3 using printStackTrace()
			
			e.printStackTrace();
		}
		
	}

}
