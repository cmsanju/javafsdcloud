package com.fls;

import java.io.File;
import java.io.FileOutputStream;

public class ByteWrite 
{
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/writetest.txt");
		
		FileOutputStream fo = new FileOutputStream(file);
		
		String msg = "Hi this is file write operation using ByteStream";
		
		fo.write(msg.getBytes());
		
		System.out.println("Done.");
	}
}
