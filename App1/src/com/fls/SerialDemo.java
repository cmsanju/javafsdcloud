package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream objs = new ObjectOutputStream(fos);
		
		Employee emp = new  Employee();
		
		emp.name = "Java";
		emp.city = "Blr";
		emp.state = "KA";
		emp.pincode = 234234;
		
		objs.writeObject(emp);
		
		System.out.println("Done.");
	}
}
