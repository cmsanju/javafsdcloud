package com.fls;

import java.io.File;
import java.io.FileWriter;

public class WriteTest {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/write.txt");
		
		FileWriter fw = new FileWriter(file);
		
		String msg = "Char Stream write operation ";
		
		fw.write(msg);
		
		fw.flush();
		
		System.out.println("Done.");
	}

}
