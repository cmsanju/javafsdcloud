package com.test;

class A
{
	int x = 20;
	String name = "java";
	
	public void show()
	{
		System.out.println("Parent");
	}
}

class B extends A
{
	
	int y = 30;
	String crs = "FSD";
	
	public void disp()
	{
		System.out.println("Child : "+x+" "+name+" "+y+" "+crs);
	}
	
	@Override
	public void show()
	{
		System.out.println("child");
	}
}

public class Exp1 {
	
	public static void main(String[] args) {
		
		B b = new B();
		
		b.show();
		b.disp();
		
	}

}
