package com.test;

class C
{
	public void human()
	{
		System.out.println("top most parent");
	}
}

class D extends C
{
	public void animal()
	{
		System.out.println("intmd base class");
	}
}

class E extends D
{
	public void dog()
	{
		System.out.println("btm most child");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		E e = new E();
		
		e.human();
		e.animal();
		e.dog();
	}

}
