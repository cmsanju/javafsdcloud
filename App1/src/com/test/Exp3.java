package com.test;

class F
{
	public void book()
	{
		System.out.println("parent");
	}
}

class G extends F
{
	public void pen()
	{
		System.out.println("child1");
	}
}

class H extends F
{
	public void color()
	{
		System.out.println("child2");
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		G g = new G();
		g.book();
		g.pen();
		
		H h = new H();
		
		h.book();
		h.color();
		
	}

}
