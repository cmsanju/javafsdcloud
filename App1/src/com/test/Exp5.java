package com.test;

class Parent
{
	public void draw()
	{
		System.out.println("tle");
	}
}

class Child1 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("Rle");
	}
}

class Child2 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("Cle");
	}
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		Child1 obj1 = new Child1();
		
		obj1.draw();
		
		Child2 obj2 = new Child2();
		
		obj2.draw();
		
		//dynamic binding
		
		Parent p1 = new Child1();
		
		p1.draw();
		
		Parent p2 = new Child2();
		
		p2.draw();
	}

}
