package com.test;


interface I1
{
	void human();
	
	default void cat()
	{
		System.out.println("default");
	}
	
	static void dog()
	{
		System.out.println("static");
	}
}

abstract class Abs
{
	public abstract void animal();
	
	public void book()
	{
		System.out.println("abs default");
	}
}

class Test extends Abs implements I1
{
	@Override
	public void animal()
	{
		System.out.println("abs overrided");
	}
	
	@Override
	public void human()
	{
		System.out.println("Inf overrided");
	}
}

public class Exp6 {
	
	public static void main(String[] args) {
		
		Test obj = new Test();
		
		obj.animal();
		obj.human();
		obj.book();
		obj.cat();
		
		I1.dog();
		
	}

}
