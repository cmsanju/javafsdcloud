package com.test;

interface J1
{
	void add();
	
	interface K1
	{
	
	  void sub();
	}
	
}

class Jimpl implements J1.K1
{
	public void sub()
	{
		System.out.println("overrided");
	}
}


public class Exp7 {
	
	public static void main(String[] args) {
		
		Jimpl obj = new Jimpl();
		
		obj.sub();
	}

}
