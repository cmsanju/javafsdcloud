package com.test;

@FunctionalInterface
interface FunInf
{
	String show();
	
	
	
}


public class Exp8 {
	
	public static void main(String[] args) {
		
		FunInf obj = new FunInf()
				{
					@Override
					public String show()
					{
						System.out.println("named object");
						
						return "java";
					}
				};
				
				obj.show();
				
		new FunInf()
		{
			@Override
			public String show()
			{
				System.out.println("nameless object");
				
				return "hello";
			}
		}.show();
		
		//from jdk 8 lambda expression
		
		FunInf obj1 = () -> {
			System.out.println("lambda expression");
			
			return "java 8";
		};
		
		obj1.show();
	}

}
