package com.test;

public class Exp9 {
	
	public static void main(String[] args) {
		
		String val = "34";
		
		int x = Integer.parseInt(val);
		
		int y = 30;
		
		double z = 34.89;
		
		String val1 = String.valueOf(y);
		String val2 = String.valueOf(z);
		
		
		//Auto boxing
		
		float f = 23.45f;
		
		Float ff = new Float(f);
		
		//Auto unboxing
		
		Double dd = new Double(464.44);
		
		double d = dd;
		
	}

}
