package com.ths;

public class Exp1 extends Thread
{
	@Override
	public void run()
	{
		try
		{
			Thread.sleep(1000);
			System.out.println("I AM FROM RUN()");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		
		Exp1 t1 = new Exp1();
		
		System.out.println("Before starting thread state : "+t1.getState());
		System.out.println("Before starting thread status : "+t1.isAlive());
		
		t1.start();
		System.out.println("after starting thread state : "+t1.getState());
		System.out.println("after startng thread status : "+t1.isAlive());
		Thread.sleep(100);
		System.out.println("In sleep thread state : "+t1.getState());
		System.out.println("In sleep thread status : "+t1.isAlive());
		
		t1.join();
		
		System.out.println("after join thread state : "+t1.getState());
		System.out.println("after join sthread status : "+t1.isAlive());
		
	}
}
