package com.ths;

public class Exp2 extends Thread
{
	
	public void run()
	{
		System.out.println("i am from run()"+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {
		
		Exp2 t1 = new Exp2();
		
		Exp2 t2 = new Exp2();
		
		Exp2 t3 = new Exp2();
		
		System.out.println("Default thread name : "+t1.getName());
		System.out.println("Default thread priority : "+t1.getPriority());
		
		System.out.println("Default thread name : "+t2.getName());
		System.out.println("Default thread name : "+t3.getName());
		
		t1.setName("Transfer");
		t2.setName("withdraw");
		t3.setName("credit");
		
		System.out.println("After thread name : "+t1.getName());
		System.out.println("After thread name : "+t2.getName());
		System.out.println("After thread name : "+t3.getName());
		
		t1.setPriority(MAX_PRIORITY);
		t2.setPriority(MIN_PRIORITY);
		
		t1.start();
		t2.start();
		t3.start();
		
		System.out.println(MAX_PRIORITY);
		System.out.println(NORM_PRIORITY);
		System.out.println(MIN_PRIORITY);
		
		
		
		System.out.println(t1.getPriority());
		System.out.println(t2.getPriority());
		System.out.println(t3.getPriority());
		
	}

}
