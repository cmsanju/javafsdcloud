package com.ths;

class Add
{
	public void add()
	{
		System.out.println("add()");
	}
}

class Sub
{
	public void sub()
	{
		System.out.println("sub()");
	}
}

public class Exp3 implements Runnable
{
	public void run()
	{
		try {
			Add a = new Add();
			
			a.add();
			
			Thread.sleep(2000);
		
			Sub s = new Sub();
		
			s.sub();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		Exp3 t1 = new Exp3();
		
		//t1.start();
		
		ThreadGroup tg1 = new ThreadGroup("Bank");
		
		
		Thread t2 = new Thread(tg1, t1, "Transfer");
		Thread t3 = new Thread(tg1, t1, "Withdraw");
		Thread t4 = new Thread(tg1, t1, "Credit");
		
		ThreadGroup tg2 = new ThreadGroup("Maths");
		
		Thread t5 = new Thread(tg2, t1, "Add");
		Thread t6 = new Thread(tg2, t1, "Sub");
		Thread t7 = new Thread(tg2, t1, "Div");
		t2.setDaemon(true);
		t2.start();
		t4.start();
		System.out.println("Tg1 active count : "+tg1.activeCount());
		t6.start();
		t7.start();
		System.out.println("Tg2 active count : "+tg2.activeCount());
	
		
		
		System.out.println(t2.isDaemon());
		
		
		
	}
}
