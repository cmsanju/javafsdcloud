package com.ths;

class Item
{
	boolean valSet = false;
	int value;
	
	public synchronized void putItem(int i)
	{
		try
		{
			if(valSet)
			{
				wait();
			}
			
			value = i;
			
			System.out.print("Producer produced one item -> "+value);
			
			valSet = true;
			
			notify();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public synchronized void getItem()
	{
		try
		{
			if(!valSet)
			{
				wait();
			}
			
			System.out.println(" -> Consumer consumed one item -> "+value);
			
			valSet = false;
			
			notify();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

class Producer extends Thread
{
	Item item;
	int val;
	
	public Producer(Item item)
	{
		this.item = item;
	}
	
	public void run()
	{
		try
		{
			while(true)
			{
				Thread.sleep(1000);
				item.putItem(val++);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

class Consumer extends Thread
{
	Item item;
	
	public Consumer(Item item)
	{
		this.item = item;
	}
	
	public void run()
	{
		try
		{
			while(true)
			{
				Thread.sleep(400);
				item.getItem();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

public class ITCDemo {
	
	public static void main(String[] args) {
		
		Item item = new Item();
		
		Producer pr = new Producer(item);
		Consumer cr = new Consumer(item);
		
		pr.start();
		cr.start();
		
	}

}
