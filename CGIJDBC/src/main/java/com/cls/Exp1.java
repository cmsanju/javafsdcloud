package com.cls;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//HashMap<String, Integer> data = new HashMap<String, Integer>();
		
		Hashtable<String, Integer> data = new Hashtable<String, Integer>();
		
		data.put("lenovo", 373);
		data.put("sony", 4455);
		data.put("asus", 8383);
		data.put("Apple", 9988);
		data.put("dell", 848);
		data.put("mac", 8883);
		data.put("sony", 7788);
		data.put("apple", 8899);
		
		System.out.println(data);
		
		LinkedHashMap<String, Integer> data1 = new LinkedHashMap<String, Integer>();
		
		data1.put("lenovo", 373);
		data1.put("sony", 4455);
		data1.put("asus", 8383);
		data1.put("Apple", 9988);
		data1.put("dell", 848);
		data1.put("mac", 8883);
		data1.put("sony", 7788);
		data1.put("apple", 8899);
		
		System.out.println(data1);
		
		Iterator<Entry<String, Integer>> itr = data1.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Product : "+et.getKey()+" Price : "+et.getValue());
		}
		
		for(String key : data1.keySet())
		{
			System.out.println(key+" "+data1.get(key));
		}
		
		
	}

}
