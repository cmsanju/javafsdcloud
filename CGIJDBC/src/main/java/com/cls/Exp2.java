package com.cls;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Exp2 {
	
	public static void main(String[] args) {
		
		TreeMap<String, Integer> data = new TreeMap<String, Integer>();
		
		data.put("lenovo", 373);
		data.put("sony", 4455);
		data.put("asus", 8383);
		data.put("Apple", 9988);
		data.put("dell", 848);
		data.put("mac", 8883);
		data.put("sony", 7788);
		data.put("apple", 8899);
		
		System.out.println(data);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Product : "+et.getKey()+" Price : "+et.getValue());
		}
	}

}
