package com.cls1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//Collection data = new ArrayList();
		//List data = new ArrayList();
		
		ArrayList data = new ArrayList();
		
		data.add(10);
		data.add(45.58f);
		data.add(30.89);
		data.add(false);
		data.add('C');
		data.add("java");
		data.add("java");
		data.add(10);
		/*
		System.out.println(data);
		
		System.out.println(data.size());
		
		//Iterator   ListIterator    Enumeration
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		
		}
		*/
		
		for(int i = 0; i < data.size(); i++) {
		
			
			if(data.lastIndexOf((data.get(i))) != i)
			{
				System.out.println(data.get(i));
			}
			
		}
		
	}

}
