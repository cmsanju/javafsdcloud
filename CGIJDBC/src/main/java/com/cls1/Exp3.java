package com.cls1;

import java.util.Stack;

public class Exp3 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(10);
		data.add("java");
		data.add(45.58f);
		data.add(30.89);
		data.add(false);
		data.add('C');
		data.add("java");
		data.add("hello");
		
		System.out.println(data);
		
		//pop() delete top element
		//peek() print top element
		//push() add element at top position
		//search() if present positive values or negative alues
		//empty() if data present false or true
		
		System.out.println(data.peek());
		
		System.out.println(data.pop());
		
		data.push("collections");
		
		System.out.println(data);
		
		System.out.println(data.search(300.89));
		
		System.out.println(data.empty());
	}

}
