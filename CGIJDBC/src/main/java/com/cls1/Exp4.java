package com.cls1;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class Exp4 {
	
	
	public static void main(String[] args) {
		
		HashSet data = new HashSet();//random display
		
		data.add(10);
		data.add(45.58f);
		data.add(30.89);
		data.add(false);
		data.add('C');
		data.add("java");
		data.add("java");
		data.add(10);
		
		System.out.println(data);
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		System.out.println();
		
		LinkedHashSet data1 = new LinkedHashSet();
		
		data1.add(10);
		data1.add("java");
		data1.add(45.58f);
		data1.add(30.89);
		data1.add(false);
		data1.add('C');
		data1.add("java");
		data1.add(10);
		
		System.out.println(data1);
	}

}
