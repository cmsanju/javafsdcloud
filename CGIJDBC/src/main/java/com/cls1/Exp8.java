package com.cls1;

import java.util.TreeMap;


public class Exp8 {
	
	public static void main(String[] args) {
		
		TreeMap<String, Integer> data = new TreeMap<String, Integer>();
		
		
		data.put("mango", 30);
		data.put("orange", 40);
		data.put("angeer", 100);
		data.put("butter", 130);
		data.put("grapes", 47);
		data.put("papaya", 50);
		data.put("apple", 100);
		
		System.out.println(data);
		
		
		for(String frts : data.keySet())
		{
			System.out.println("Fruit : "+frts+" Price : "+data.get(frts));
		}
		
		
	}

}
