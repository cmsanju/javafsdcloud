package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//step 1 load the driver class
		
		Class.forName("com.mysql.jdbc.Driver");
		
		//step 2 create connection object
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/cgib1", "root", "password");
		
		//step 3 create statement object
		
		Statement stmt = con.createStatement();
		
		// step 4 execute the query
		
		//stmt.execute("create table emp101(id int, name varchar(50), city varchar(50))");
		
		String sql1 = "insert into emp101 values(4, 'CGI', 'blr')";
		
		String sql2 = "update emp101 set name = 'Satyam' where id = 1";
		
		String sql3 = "delete from emp101 where id = 5";
		
		stmt.addBatch(sql1);
		stmt.addBatch(sql2);
		stmt.addBatch(sql3);
		
		stmt.executeBatch();
		
		//step 5 close the connection object
		
		con.close();
		
		System.out.println("Done.");
		
		
	}

}
