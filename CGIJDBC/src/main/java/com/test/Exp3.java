package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class Exp3 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/cgib1", "root", "password");
		
		con.setAutoCommit(false);
		/*
		PreparedStatement pst = con.prepareStatement("insert into emp101 values(?,?,?)");
		
		pst.setInt(1, 5);
		pst.setString(2, "nILES");
		pst.setString(3, "Blr");
		
		pst.execute();
		
		
		PreparedStatement pst = con.prepareStatement("update emp101 set name = ? where id = ?");
		
		pst.setString(1, "Nilesh");
		pst.setInt(2, 5);
		
		
		PreparedStatement pst = con.prepareStatement("delete from emp101 where id = ?");
		
		pst.setInt(1, 4);
		*/
		
		PreparedStatement pst = con.prepareStatement("select * from emp101");
		
		ResultSet rs = pst.executeQuery();
		
		ResultSetMetaData rsd = rs.getMetaData();
		
		System.out.println(rsd.getColumnCount());
		System.out.println(rsd.getColumnClassName(1));
		System.out.println(rsd.getTableName(3));
		
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		con.commit();
		
		
		System.out.println("Done.");
		
		con.close();
	}

}
