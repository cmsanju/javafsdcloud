package com.dts;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.List;

//DateTime API

public class Exp1 {
	
	public static void main(String[] args) {
		
		LocalDate date = LocalDate.now();
		
		System.out.println(date);
		
		LocalDate date1 = LocalDate.of(2020, Month.DECEMBER, 3);
		
		System.out.println(date1);
		
		LocalDate date2 = LocalDate.now(ZoneId.of("Asia/Kolkata"));
		
		System.out.println(date2);
		
		LocalDate date3 = LocalDate.ofYearDay(2021, 100);
		
		System.out.println(date3);
		
		LocalDate date4 = date.minusDays(1);
		
		System.out.println(date4);
		
		LocalDate date5 = date.plusDays(20);
		
		System.out.println(date5);
		
		System.out.println(date1.isLeapYear());
		
		String dateInp = "2021-12-03";
		
		LocalDate date6 = LocalDate.parse(dateInp);
		
		System.out.println(date6);
		
		//LocalDateTime 
		
		
	}

}
