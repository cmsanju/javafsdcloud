package com.dts;

import java.text.Format;
import java.text.SimpleDateFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class Exp2 {
	
	public static void main(String[] args) {
		
		LocalTime ctime = LocalTime.now();
		
		System.out.println(ctime);
		
		LocalTime time = LocalTime.of(9, 55,33);
		
		System.out.println(time.plusHours(5));
		
		System.out.println(time.minusHours(10));
		
		ZoneId ist = ZoneId.of("Asia/Kolkata");
		
		ZoneId jpn = ZoneId.of("Asia/Tokyo");
		
	//	ZoneId z1 = ZoneId.of("USA/Chicago");
		
		LocalTime time1 = LocalTime.now(ist);
		
		LocalTime time2 = LocalTime.now(jpn);
		
		//LocalTime time3 = LocalTime.now(z1);
		
		System.out.println(time1);
		System.out.println(time2);
		//System.out.println(time3);
		
		LocalDateTime ldt =  LocalDateTime.now();
		
		System.out.println(ldt);
		
		System.out.println(ldt.getDayOfMonth());
		
	
		
		DateTimeFormatter frmt = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		
		
		
		System.out.println(frmt);
		
		System.out.println(ldt.format(frmt));
		
		Format fmt = new SimpleDateFormat("EEEE");
		
		String name = fmt.format(new Date());
		
		System.out.println(name);
		
		LocalDate date1 = LocalDate.of(2020, Month.DECEMBER, 3);
		
		Calendar cl = Calendar.getInstance();
		
		cl.set(Calendar.DAY_OF_MONTH, 11);
		cl.set(Calendar.MONTH,12);
		cl.set(Calendar.YEAR, 2021);
		System.out.println(cl.get(Calendar.DAY_OF_WEEK));
		 	
	}

}
