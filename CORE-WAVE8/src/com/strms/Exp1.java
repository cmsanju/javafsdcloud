package com.strms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Exp1 {
	
	/**
	 * intermediate operations
	 * 
	 * 1 filter 
	 * 2 map
	 * 3 sorted
	 * 
	 * terminal operations
	 * .collect
	 * .forEach
	 * .reduce
	 */

	
	public static void main(String[] args) {
		
		//List<Integer> listNum = new ArrayList<>();
		List<Integer> listNum = Arrays.asList(77,66,34,90,12,34,56,78);
		
		for(Integer j : listNum)
		{
			if(j == 90)
			System.out.println(j+j);
		}
		
		List<Integer> flt = listNum.stream().map(k -> k+k).collect(Collectors.toList());
		
		System.out.println(flt);
		
		List<String> lstNames = Arrays.asList("java","java", ".net", "php", "php", "python","spring", "hibernate", "core");
		
		
		List<String> fltNames = lstNames.stream().filter(t -> t.startsWith("p")).collect(Collectors.toList());
		
		System.out.println(fltNames);
		
		List<String> srtNames = lstNames.stream().sorted().collect(Collectors.toList());
		
		System.out.println(srtNames);
		
		Set<String> unqData = lstNames.stream().collect(Collectors.toSet());
		
		System.out.println(unqData);
		
		List<String> lstUnq = lstNames.parallelStream().distinct().collect(Collectors.toList());
		
		System.out.println(lstUnq);
		
		lstUnq.forEach(l -> System.out.println(l));
		
		List<String> data = Arrays.asList("Java", "Spring", "Jsp", "Jsp", null);
		
		Optional<String> rdata = data.stream().reduce((names1, names2)->names1 +" : "+names2);
		
		System.out.println(rdata);
		
	}
}
