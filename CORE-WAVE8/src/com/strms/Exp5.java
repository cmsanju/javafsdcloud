package com.strms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Employee implements Comparable<Employee>
{
	private int id;
	private String name;
	private double salary;
	
	
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, double salary)
	{
		this.id = id;
		this.name = name;
		this.salary = salary;
		
	}
	
	public int compareTo(Employee emp)
	{
		return this.id - emp.id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public double getSalary() {
		return salary;
	}	
}

class NameComparator implements Comparator<Employee>
{
	public int compare(Employee e1, Employee e2)
	{
		return e1.getName().compareTo(e2.getName());
	}
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		ArrayList<Employee> empList = new ArrayList<Employee>();
		
		empList.add(new Employee(11,"java", 23.33));
		empList.add(new Employee(2,"hero", 23.33));
		empList.add(new Employee(1,"spring", 23.33));
		empList.add(new Employee(5,"apple", 23.33));
		
		//Comparable
		
		Collections.sort(empList);
		
		for(Employee emp : empList)
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getSalary());
		}
		
		//Comparator
		
		Collections.sort(empList, new NameComparator());
		
		for(Employee e : empList)
		{
			System.out.println(e.getId()+" "+e.getName()+" "+e.getSalary());
		}
		
	}

}
