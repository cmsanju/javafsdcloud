package com.cls;

import java.util.TreeMap;

class Employee1
{
	private int id;
	private String name,city;
	
	public Employee1()
	{
		
	}
	
	public Employee1(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
}

public class Exp10 {
	
	public static void main(String[] args) {
		
		Employee1 e1 = new Employee1(1, "Pavan", "Mpl");
		
		Employee1 e2 = new Employee1(2, "Nagendra", "NGP");
		
		TreeMap<Integer, Employee1> data = new TreeMap<Integer, Employee1>();
		
		data.put(10, e1);
		data.put(5, new Employee1(3, "Ragava", "NTRc"));
		data.put(11, e2);
		
		System.out.println(e1);
		
		System.out.println(e1.getId()+" "+e1.getName()+" "+e1.getCity());
		
		
		for(Employee1 emp : data.values())
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getCity());
		}
		
	}

}
