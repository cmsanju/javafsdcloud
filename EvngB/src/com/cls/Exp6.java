package com.cls;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Exp6 {
	
	public static void main(String[] args) {
		
		ArrayList<String> data1 = new ArrayList<String>();
		
		data1.add("zero");
		data1.add("lenovo");
		data1.add("dell");
		data1.add("sony");
		data1.add("asus");
		data1.add("think pad");
		data1.add("macos");
		data1.add("apple");
		data1.add("dell");
		data1.add("sony");
		data1.add("dell");
		data1.add("sony");
		
		Set<String> data = new HashSet<String>();
		
		for(String list : data1)
		{
			if(data.add(list) == false)
			{
				System.out.println(list);
			}
		}
	}

}
