package com.cls;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;

import java.util.Map;
import java.util.Map.Entry;

public class Exp7 {
	
	public static void main(String[] args) {
		
		//Map<Integer, String> data = new HashMap<>();
		Map<Integer, String> data = new Hashtable<>();
		
		data.put(12, "java");
		data.put(1, ".net");
		data.put(3, "php");
		data.put(5, "spring");
		data.put(3, "Hibernate");
		data.put(12, "oracle");
		
		System.out.println(data);
		
		
		LinkedHashMap<Integer, String> data1 = new LinkedHashMap<Integer, String>();
		
		data1.put(12, "java");
		data1.put(1, ".net");
		data1.put(3, "php");
		data1.put(5, "spring");
		data1.put(3, "Hibernate");
		data1.put(12, "oracle");
		data1.put(34, "apple");
		
		System.out.println(data1);
		
		Iterator<Entry<Integer, String>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<Integer, String> et = itr.next();
			
			System.out.println("Key : "+et.getKey()+" Value : "+et.getValue());
		}
		
		for(Integer key : data1.keySet())
		{
			System.out.println("Key : "+key+" Value : "+data1.get(key));
		}
	}

}
