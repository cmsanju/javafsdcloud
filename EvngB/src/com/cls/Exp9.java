package com.cls;

import java.util.TreeMap;

class Employee
{
	private int id;
	private String name,city;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
}

public class Exp9 {
	
	public static void main(String[] args) {
		
		TreeMap<Integer, Employee> data = new TreeMap<Integer, Employee>();
		
		Employee e1 = new Employee(2, "hello", "blr");
		
		data.put(401, new Employee(1, "Hero", "mpl"));
		
		data.put(101, e1);
		
		data.put(301, new Employee(5, "Nag", "Agl"));
		
		
		System.out.println(data);
		
		for(Employee emp : data.values())
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getCity());
		}
		
		
	}

}
