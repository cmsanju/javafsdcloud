package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//step1 : load the driver class
		
		Class.forName("com.mysql.jdbc.Driver");
		
		//step2 : create connection object 
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/evngjava", "root", "password");
		
		//step3 : create statement object
		
		Statement stmt = con.createStatement();
		
		//step4 : execute the query
		
		//stmt.execute("create table emp1(id int, emp_name varchar(25), city varchar(25))");
		
		String sql1 = "insert into emp1 values(4, 'nARESH', 'AGL')";
		String sql4 = "insert into emp1 values(5, 'Ragava', 'mpl')";
		
		String sql2 = "update emp1 set emp_name = 'Rakesh', city = 'NGP' where id = 1";
		
		String sql3 = "delete from emp1 where id = 4";
		
		stmt.addBatch(sql1);
		stmt.addBatch(sql4);
		stmt.addBatch(sql2);
		stmt.addBatch(sql3);
		
		stmt.executeBatch();
		
		String sql = "select * from emp1";
		
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		//System.out.println("Done.");
		
		//step5 : close the connection object
		
		con.close();
	}

}
