package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Exp2 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/evngjava", "root", "password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into emp1 values(?,?,?)");
		
		pst.setInt(1, 4);
		pst.setString(2, "Ragava");
		pst.setString(3, "MPL");
		
		pst.execute();
		
		
		PreparedStatement pst = con.prepareStatement("update emp1 set emp_name = ? where id = ? ");
		
		pst.setString(1, "Ganesh");
		pst.setInt(2, 3);
		pst.execute();
		
		
		PreparedStatement pst = con.prepareStatement("delete from emp1 where id = ?");
		
		pst.setInt(1, 4);
		
		pst.execute();
		*/
		
		PreparedStatement pst = con.prepareStatement("select * from emp1");
		
		ResultSet rs = pst.executeQuery();
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		
		//System.out.println("Done.");
		
		con.close();
	}

}
