package com.db;
import com.mongodb.client.MongoDatabase;

import java.util.List;
import java.util.Set;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
public class Exp3 {
   public static void main( String args[] ) {
      // Creating a Mongo client
      MongoClient mongo = new MongoClient( "localhost" , 27017 );
      // Creating Credentials
      MongoCredential credential;
      credential = MongoCredential.createCredential("sampleUser", "EmployeeDB", "password".toCharArray());
      System.out.println("Connected to the database successfully");
      //Accessing the database
      MongoDatabase database = mongo.getDatabase("EmployeeDB");
      //Creating a collection
     // database.createCollection("sampleCollection");
     // System.out.println("Collection created successfully");
      
      List<String> databases = mongo.getDatabaseNames();
      
      for (String dbName : databases) {
          System.out.println("- Database: " + dbName);
           
          DB db = mongo.getDB(dbName);
           
          Set<String> collections = db.getCollectionNames();
          for (String colName : collections) {
              System.out.println("\t + Collection: " + colName);
          }
      }
       
      mongo.close();
   }
}