package com.excp;

public class Exp2 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(200/2);
			
			String str = "java";
			
			System.out.println(str.charAt(3));
			
			String[] ar = {"java", ".net", "php"};
			
			System.out.println(ar[2]);
			
			String name = null;
			
			System.out.println(name.length());
		}
		catch(ArithmeticException ae)
		{
			System.out.println("don't enter zero for den");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check your name length");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check your array size");
		}
		catch(NullPointerException npe)
		{
			System.out.println("pelase enter name");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		
		
		finally
		{
			System.out.println("I am from finally");
		}
	}

}
