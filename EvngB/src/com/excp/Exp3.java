package com.excp;

public class Exp3 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println("Hi");
		
			System.out.println(30/0);
		
			System.out.println("Hello");
		
			//how to deal with unknown exceptions
		}
		catch(Exception e)
		{
			//1 using getMessage()
			
			System.out.println(e.getMessage());//only message
			
			//2 printing exception class message
			
			System.out.println(e);// exception class name message
			
			//3 using pritStackTrace()
			
			e.printStackTrace();
		}
	}
}
