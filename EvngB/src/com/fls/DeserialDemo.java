package com.fls;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserialDemo {
	
	public static void main(String[] args) //throws Exception
	{
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try
		{
		fis = new FileInputStream("src/employee.txt");
		
		ois = new ObjectInputStream(fis);
		
		Employee emp = (Employee)ois.readObject();
		
		System.out.println("ID : "+emp.id+" Name : "+emp.name+" City : "+emp.city+" Pincode : "+emp.pincode);
	
		
		//ois.close();
		
		//fis.close();
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(fis == null && ois == null)
			{
				try {
					
					ois.close();
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
		}
		
		
	
	}

}
