package com.fls;

import java.io.File;
import java.io.FileOutputStream;

public class FileWrite 
{
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/sample.txt");
		
		System.out.println(file.isFile());
		
		FileOutputStream fos = new FileOutputStream(file);
		
		String msg = "Hi this bytestream write operation example";
		
		fos.write(msg.getBytes());
		
		System.out.println("Success.");
		
	}
}
