package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import java.applet.*;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream ost = new ObjectOutputStream(fos);
		
		Employee e = new Employee();
		
		e.id = 101;
		e.name = "Java";
		e.city = "MPL";
		e.pincode = 123123;
		
		ost.writeObject(e);
		
		System.out.println("Success.");
		
	}

}
