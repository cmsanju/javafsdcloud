package com.fls;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class WriteTest {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/evng.txt");
		
		FileWriter fw = new FileWriter(file);
		
		BufferedWriter bw = new BufferedWriter(fw);
		
		String msg = "This is charstream write operation example";
		
		bw.write(msg);
		
		bw.flush();
		
		System.out.println("Success.");
		
		
	}

}
