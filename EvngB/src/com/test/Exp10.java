package com.test;

interface InfX
{
	void add();
	
	interface InfY
	{
	 void sub();
	}
}

class Impl1 implements InfX.InfY
{
	public void sub()
	{
		System.out.println("sub overrided");
	}
}

public class Exp10 {
	
	public static void main(String[] args) {
		
		Impl1 obj = new Impl1();
		
		obj.sub();
	}
}
