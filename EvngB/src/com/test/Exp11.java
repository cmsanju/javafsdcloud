package com.test;

@FunctionalInterface
interface FunTest
{
	void draw();
	
}

public class Exp11 {
	
	public static void main(String[] args) {
		
		FunTest obj = new FunTest()
				{
					public void draw()
					{
						System.out.println("Hello User");
					}
				};
				
				obj.draw();
				
		// java 8 new feature lambda expression 
				
	   FunTest obj1 = () -> System.out.println("Hi this is lambda");
	   
	   obj1.draw();
	}

}
