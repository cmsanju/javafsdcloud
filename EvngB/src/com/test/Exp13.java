package com.test;

public class Exp13 {
	
	public static void main(String[] args) {
		
		//AUTO-BOXING
		
		double a = 34.78;
		
		Double d = new Double(a);
		
		int b = 87;
		
		Integer e = new Integer(b);
		
		//AUTO-UNBOXING 
		
		Integer i1 = new Integer(300);
		
		int i2 = i1;
	}

}
