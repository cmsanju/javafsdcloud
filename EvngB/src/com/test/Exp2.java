package com.test;

import java.util.Stack;

public class Exp2 {
	
	private String make;
	private String model;
	
	public Exp2(String make, String model)
	{
		this.make = make;
		this.model = model;
	}
	
	public int hashCode() {
		return make.length()*10+model.length();
	}
	
	public boolean equals(Exp2 other)
	{
		return make.equals(other.make) && model.equals(other.model);
	}
	
	public static void main(String[] args) {
		
		Exp2 a = new Exp2("Philips", "42PFL5603D");
		Exp2 b = new Exp2("Philips", "42PFL5603D1");
		
		if(a.equals(b))
		{
			System.out.println("equals");
		}
		else
		{
			System.out.println("not equals");
		}
		
	}

}
