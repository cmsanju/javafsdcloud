package com.test;


class A
{
	int id = 101;
	String name = "java";
	
	public void disp()
	{
		System.out.println("parent class method");
	}
}

class B extends A
{
	String city = "MPL";
	
	public void show()
	{
		System.err.println(id+" "+name+" "+city);
	}
}


public class Exp3 {
	
	public static void main(String[] args) {
		
		
		B b = new B();
		b.disp();
		b.show();
	}

}
