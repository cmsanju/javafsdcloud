package com.test;

class Student
{
	public Student()
	{
		this(5);
		System.out.println("Default");
	}
	
	public Student(int x)
	{
		this("hello");
		System.out.println("parameterised");
		//this("hshs"); wrong position 
	}
	
	public Student(String name)
	{
		
		//this();
		System.out.println("overloaded");
	}
	
	public Student(Student obj)
	{
		System.out.println("object parameterised");
	}
	
	//factory method
	
	public Student getInstance(Student obj)
	{
		System.out.println("object created");
		
		return obj;
	}
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		Student std1 = new Student();
		
		//Student std2 = new Student(30);
		
		//Student std3 = new Student("hello");
		
		//Student std4 = new Student(std1);
		
		std1.getInstance(std1);
		
	}

}
