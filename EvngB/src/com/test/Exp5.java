package com.test;

class Book
{
	public void disp()
	{
		System.out.println("Hello display");
	}
	
	public int hello()
	{
		System.out.println("hello user");
		
		return 1;
	}
	
	public Book createObj(Book obj)
	{
		System.out.println("Factory method object create");
		
		return obj;
	}
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		Book obj = new Book();
		
		obj.disp();
		
		int x = obj.createObj(obj).hashCode();
		
		System.out.println(x);
		System.out.println(obj.hashCode());
		
		/*
		 * How many ways we can create objects
		 * 1 new operator
		 * 2 factory method
		 * 3 clone()
		 * 4 Class.forName() dynamic object creation 
		 * 
		 */
		
	}

}
