package com.test;

class C
{
	public void cat() {
		System.out.println("from c class");
	}
}

class D extends C
{
	public void dog()
	{
		System.out.println("from d class");
	}
}

class E extends D
{
	public void animal()
	{
		System.out.println("from e class");
	}
}



public class Exp6 {
		
	public static void main(String[] args) {
		
		E e = new E();
		
		e.cat();
		e.dog();
		e.animal();
		
	}
	
}
