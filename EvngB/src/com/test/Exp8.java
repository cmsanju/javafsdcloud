package com.test;

class Parent
{
	public void draw()
	{
		System.out.println("Tle");
	}
	//ac.no fixed
}

class Child1 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("Cle");
	}
	
	//ac.no address modified 
}

class Child2 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("Rle");
	}
}

public class Exp8 {
	
	public static void main(String[] args) {
		
		Child1 c1 = new Child1();
		
		c1.draw();
		
		Child2 c2 = new Child2();
		
		c2.draw();
		
		Parent p1 = new Child1();// indirect object creation or dynamic binding
		
		p1.draw();
		
		Parent p2 = new Child2();
		
		p2.draw();
		
	}

}
