package com.test;

interface Inf1
{
	void cat();
	void dog();
	
	default void human()
	{
		System.out.println("human method from inf");
	}
	
	static void animal()
	{
		System.out.println("animal method from inf");
	}
	
}

abstract class Abs
{
	public abstract void show();
	
	public void movie()
	{
		System.out.println("abs normal method");
	}
}

class Impl extends Abs implements Inf1
{
	public void show()
	{
		System.out.println("abs overrided");
	}
	
	public void cat()
	{
		System.out.println("inf method cat overrided");
	}
	
	public void dog()
	{
		System.out.println("inf method dog overrided");
	}
}

public class Exp9 {
	
	public static void main(String[] args) {
		
		//Inf1 obj = new Inf1();
		//Abs obj = new Abs();
		
		Impl obj = new Impl();
		
		obj.show();
		obj.dog();
		obj.cat();
		obj.movie();
		obj.human();
		
		Inf1.animal();
		
	}

}
