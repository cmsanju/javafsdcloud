package com.test1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CountEachWords 
{
	static void CountWords(String filename, Map<String, Integer> words) throws FileNotFoundException
	{
		Scanner file = new Scanner(new File(filename));
		while (file.hasNext()) {
			String word = file.next();
			Integer count = words.get(word);
			if (count != null)
				count++;
			else
				count = 1;
			words.put(word, count);
		}
		file.close();
	}

	public static void main(String[] args) throws Exception
	{
		Map<String, Integer> words = new HashMap<String, Integer>();
		CountWords("src/shubham.txt", words);
		System.out.println(words);
	}
}
