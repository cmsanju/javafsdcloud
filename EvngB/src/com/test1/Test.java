package com.test1;

import java.util.ArrayList;
import java.util.List;

class Hello
{
	public static void m1()
	{
		System.out.println(10);
		return;
	}
}

class hi extends Hello
{
	public static void m1()throws ClassCastException
	{
		System.out.println("M1 in Hello");
	}
}

public class Test {

	public static void main(String[] args) {
		
		Hello h = new Hello();
		
		h.m1();
		
	}
	
	
}
