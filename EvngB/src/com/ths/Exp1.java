package com.ths;

class Add
{
	public void add()
	{
		System.out.println(34+90);
	}
}

class Sub
{
	public void sub()
	{
		System.out.println(50-30);
	}
}

public class Exp1 extends Thread
{
		@Override
		public void run()
		{
			try {
			Add obj = new Add();
			
			obj.add();
			
			Thread.sleep(2000);
			Sub obj1 = new Sub();
			
			obj1.sub();
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		public static void main(String[] args) {
			
			Exp1 t1 = new Exp1();
			
			Exp1 t2 = new Exp1();
			
			Exp1 t3 = new Exp1();
			
			t1.start();
			
			System.out.println("MIN : "+MIN_PRIORITY);
			System.out.println("NORM : "+NORM_PRIORITY);
			System.out.println("MAX : "+MAX_PRIORITY);
			
			System.out.println("Default Thread Name : "+t1.getName());
			System.out.println("Default Thread Name : "+t2.getName());
			System.out.println("Default Thread Name : "+t3.getName());
			
			t1.setName("game");
			t2.setName("Bank");
			t3.setName("message service");
			
			System.out.println("After Thread Name : "+t1.getName());
			System.out.println("after Thread Name : "+t2.getName());
			System.out.println("after Thread Name : "+t3.getName());
			
			System.out.println("Default Thread Priority : "+t1.getPriority());
			System.out.println("Default Thread Priority : "+t2.getPriority());
			System.out.println("Default Thread Priority : "+t3.getPriority());
			
			t1.setPriority(MIN_PRIORITY);
			t3.setPriority(MAX_PRIORITY);
			
			System.out.println("Thread priority : "+t1.getPriority());
			System.out.println("Thread priority : "+t3.getPriority());
		}
}
