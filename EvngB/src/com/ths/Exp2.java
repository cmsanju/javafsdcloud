package com.ths;

public class Exp2 implements Runnable
{
	@Override
	public void run()
	{
		String name = Thread.currentThread().getName();
		
		System.out.println("i am from run() : "+name);
	}
	
	public static void main(String[] args) {
		
		Exp2 t1 = new Exp2();
		
		//t1.start();//wrong
		
		Thread t11 = new Thread(t1);//converting Runnable interface object into thread class object
		
		ThreadGroup tg1 = new ThreadGroup("Bank");
		
		Thread t2 = new Thread(tg1, t1, "Transfer");
		Thread t3 = new Thread(tg1, t1, "Credit");
		Thread t4 = new Thread(tg1, t1, "Withdraw");
		
		ThreadGroup tg2 = new ThreadGroup("Maths");
		
		Thread t5 = new Thread(tg2, t1, "Add");
		Thread t6 = new Thread(tg2, t1, "Sub");
		Thread t7  = new Thread(tg2, t1,"Div");
		
		t2.start();
		t4.start();
		System.out.println("Bank active : "+tg1.activeCount());
		t7.start();
		t6.start();
		System.out.println("Maths active : "+tg2.activeCount());
		
		
		System.out.println(t2.getName());
		
		System.out.println(t2.getPriority());
		System.out.println(tg1.getMaxPriority());
	}
}
