package com.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloService {
	
	@GET
	@Path("/text")
	@Produces(MediaType.TEXT_PLAIN)
	public String sayHelloPlaintText()
	{
		return "Hi this is plain text response";
	}
	
	@GET
	@Path("/html/{name}")
	@Produces(MediaType.TEXT_HTML)
	public String sayHelloHtml(@PathParam("name") String name)
	{
		return "<html><body><h1>Hi this simple html response "+name+"</h1></body></html>";
	}
	
	
}
