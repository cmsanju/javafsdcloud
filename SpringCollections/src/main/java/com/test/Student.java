package com.test;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class Student {
	
	private int id;
	
	private String name;
	
	private String inst;
	
	private List<String> course;
	
	@Autowired
	private Address adr;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInst() {
		return inst;
	}

	public void setInst(String inst) {
		this.inst = inst;
	}
	
	public Address getAdr() {
		return adr;
	}

	public void setAdr(Address adr) {
		this.adr = adr;
	}
	
	public List<String> getCourse() {
		return course;
	}

	public void setCourse(List<String> course) {
		this.course = course;
	}
	
	public void details()
	{
		System.out.println("ID : "+id+" Name : "+name+" Institute : "+inst+" Course : "+course);
		
		//Iterator<String> itr = course.iterator();
		
		for(String crs : course)
		{
			System.out.println(" "+crs);
		}
		
		adr.disp();
	}

	

}
