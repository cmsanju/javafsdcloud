package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Employee obj = (Employee)ctx.getBean("emp");
		
		obj.disp();
		
		Employee obj1 = (Employee)ctx.getBean("emp1");
		
		obj1.disp();
	}

}
