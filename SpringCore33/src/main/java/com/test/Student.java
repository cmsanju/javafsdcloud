package com.test;

import java.util.List;
import java.util.Map;

public class Student {
	
	private int id;
	
	private String name;
	
	private List<String> crs;
	
	private Map<String, Integer> data;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getCrs() {
		return crs;
	}

	public void setCrs(List<String> crs) {
		this.crs = crs;
	}
	
	public void disp()
	{
		System.out.println(id+" "+name+" ");
		
		for(String data : crs)
		{
			System.out.println(data);
		}
	}

}
