package com.test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

public class EmpDao {
	
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int save(Employee emp)
	{
		String sql = "insert into emp11 values("+emp.getId()+", '"+emp.getName()+"', '"+emp.getSalary()+"')";
		
		return jdbcTemplate.update(sql);
	}
	
	public int update(Employee emp)
	{
		String sql = "update emp11 set name = '"+emp.getName()+"', salary = "+emp.getSalary()+" where id = "+emp.getId()+"";
		
		return jdbcTemplate.update(sql);
	}
	
	public int delete(Employee emp)
	{
		String sql = "delete from emp11 where id ="+emp.getId()+" ";
		
		return jdbcTemplate.update(sql);
	}
	
	public List<Employee> listEmployees()
	{
		return jdbcTemplate.query("select * from emp11", new ResultSetExtractor<List<Employee>>()
				{
					public List<Employee>extractData(ResultSet rs) throws SQLException, DataAccessException
					{
						List<Employee> list = new ArrayList<Employee>();
						
						while(rs.next())
						{
							Employee e = new Employee();
							
							e.setId(rs.getInt(1));
							e.setName(rs.getString(2));
							e.setSalary(rs.getDouble(3));
							
							list.add(e);
						}
						return list;
					}
				});
	}

}
