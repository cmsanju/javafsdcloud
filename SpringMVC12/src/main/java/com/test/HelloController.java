package com.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController 
{
	@RequestMapping("/hello")
	public String sayhello()
	{
		return "greet";
	}
	
	
	@RequestMapping("/helloagain")
	public String sayhelloAgain()
	{
		return "again";
	}
}
