package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String fn = request.getParameter("fname");
		String usr = request.getParameter("user");
		String pass = request.getParameter("pwd");
		
		if(usr.contains("admin"))
		{
			RequestDispatcher rd = request.getRequestDispatcher("login.html");
			
			rd.forward(request, response);
		}
		else
		{
			out.println("<font color ='red'>Registration failed.</font>");
			
			RequestDispatcher rd = request.getRequestDispatcher("register.html");
			rd.include(request, response);
		}
	}

}
