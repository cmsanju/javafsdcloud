<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
		<h1>simple JSP Page</h1>
		
		<!-- declaration tag -->
		
		<%!
			int x = 10;
		    int y = 29;
		    
		    public int add()
		    {
		    	return x+y;
		    }
		%>
		
		<!-- expression tag -->
		
		<%= add()%>
		
		<%
		
			Date date = new Date();
		
		   out.println(date);
			
		%>
</body>
</html>