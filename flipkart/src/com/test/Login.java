package com.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String name = request.getParameter("user");
		String pass = request.getParameter("pwd");
		
		User u = new User();
		
		u.setUserName(name);
		u.setPassword(pass);
		
		try {
		
				Connection con = GetCon.getCon();
				
				//PreparedStatement pst = con.prepareStatement("select * from user121 where password=?");
				
				
				Statement stmt = con.createStatement();
				
				String sql = "select username, password from user121 where password ='"+pass+"' ";
				
				ResultSet rs = stmt.executeQuery(sql);
				
				while(rs.next()) {
					if(u.getUserName().equals(rs.getString(1)) && u.getPassword().equals(rs.getString(2)))
					{
						response.sendRedirect("https://www.google.com/");
					}
					else
					{
						out.println("<font color ='red'>Invalid username and password</font><br>");
			
						RequestDispatcher rd = request.getRequestDispatcher("user.jsp");
						rd.include(request, response);
					}
				}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
