package com.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String fname = request.getParameter("fname");
		String user = request.getParameter("user");
		String pass = request.getParameter("pwd");
		
		User usr = new User();
		
		usr.setFname(fname);
		usr.setUserName(user);
		usr.setPassword(pass);
		
		try
		{
			
			Connection con = GetCon.getCon();
			
			PreparedStatement pst = con.prepareStatement("insert into user121 values(?,?,?)");
			
			pst.setString(1, usr.getFname());
			pst.setString(2, usr.getUserName());
			pst.setString(3, usr.getPassword());
			
			boolean b = pst.execute();
			
			if(!b)
			{
				out.println("Registered successfully.");
			}
			else
			{
				out.println("Registration failed.");
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
